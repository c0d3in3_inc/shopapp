package com.c0d3in3.shopapp.utils

import android.content.Context
import com.c0d3in3.shopapp.App

object UserPreferences {

    const val USER_REF = "USER"

    private val preferences = App.getInstance().applicationContext.getSharedPreferences("USER", Context.MODE_PRIVATE)
    private val editor = preferences.edit()

    fun readString(key : String) = preferences.getString(key, "")

    fun removeString(key : String){
        if(preferences.contains(key)){
            editor.remove(key)
            editor.commit()
        }
    }

    fun insertString(key : String, value: String) {
        editor.putString(key, value)
        editor.commit()
    }
}