package com.c0d3in3.shopapp.network

interface FutureCallback {
    fun onSuccess(response: String, status: Int)
    fun onError(response: String, status: Int)
    fun onFail(response: String)
}