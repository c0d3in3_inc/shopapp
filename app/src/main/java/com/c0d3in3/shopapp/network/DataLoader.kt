package com.c0d3in3.shopapp.network

import android.view.View
import okhttp3.OkHttpClient
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.*

object DataLoader {

    private const val BASE_URL = "https://ktorhighsteaks.herokuapp.com/"

    const val REGISTER = "register"
    const val LOG_IN = "login"


    private const val HTTP_200_OK = 200
    private const val HTTP_201_CREATED = 201
    private const val HTTP_400_BAD_REQUEST = 400
    private const val HTTP_401_UNAUTHORIZED = 401
    private const val HTTP_404_NOT_FOUND = 404
    private const val HTTP_500_INTERNAL_SERVER_ERROR = 500
    private const val HTTP_204_NO_CONTENT = 204

    private val httpClient = OkHttpClient.Builder()
        .build()

    private val retrofit = Retrofit.Builder()
        .addConverterFactory(ScalarsConverterFactory.create())
        .baseUrl(BASE_URL)
        .client(httpClient)
        .build()
    private val service = retrofit.create(NetworkService::class.java)

    interface NetworkService {
        @GET
        fun getRequest(@Url path: String): Call<String>

        @FormUrlEncoded
        @POST("{path}")
        fun postRequest(@Path("path") path: String, @FieldMap params: Map<String, String>): Call<String>
    }


    fun getRequest(view: View? = null, path: String, callback: FutureCallback) {
        val call = service.getRequest(path)
        call.enqueue(onCallback(view,callback))

    }

    fun postRequest(view: View? = null, path: String, params: MutableMap<String, String>, callback: FutureCallback ) {
        if(view != null) view.visibility = View.VISIBLE
        val call = service.postRequest(path, params)
        call.enqueue(onCallback(view, callback))
    }


    private fun onCallback(view: View?, callback: FutureCallback): Callback<String> =
        object : Callback<String> {
            override fun onResponse(call: Call<String>, response: Response<String>) {
                if(view != null) view.visibility = View.GONE
                val status = response.code()
                println(response.raw())
                println(response.body().toString())

                if (status == HTTP_200_OK || status == HTTP_201_CREATED) callback.onSuccess(
                    response.body().toString(),
                    status
                )
                else if (status == HTTP_400_BAD_REQUEST) callback.onError("Bad request", status)
                else if (status == HTTP_204_NO_CONTENT) callback.onError("No content", status)
                else if (status == HTTP_401_UNAUTHORIZED) callback.onError(
                    "Unauthorized please log in again",
                    status
                )
                else if (status == HTTP_404_NOT_FOUND) callback.onError(
                    "Resource not found",
                    status
                )
                else if (status == HTTP_500_INTERNAL_SERVER_ERROR) callback.onError(
                    "Internal server error",
                    status
                )
                else callback.onError("Failed loading data", status)
            }

            override fun onFailure(call: Call<String>, t: Throwable) {
                if(view != null) view.visibility = View.GONE
                callback.onFail(t.toString())
            }
        }
}