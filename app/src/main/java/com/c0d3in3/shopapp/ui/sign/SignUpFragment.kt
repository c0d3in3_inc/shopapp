package com.c0d3in3.shopapp.ui.sign

import android.widget.Toast
import androidx.core.content.ContextCompat
import com.c0d3in3.shopapp.App
import com.c0d3in3.shopapp.BaseFragment
import com.c0d3in3.shopapp.R
import com.c0d3in3.shopapp.extensions.isEmailValid
import com.c0d3in3.shopapp.extensions.setColor
import com.c0d3in3.shopapp.network.DataLoader
import com.c0d3in3.shopapp.network.FutureCallback
import com.c0d3in3.shopapp.utils.Tools
import kotlinx.android.synthetic.main.activity_sign.*
import kotlinx.android.synthetic.main.fragment_sign_up.view.*
import kotlinx.android.synthetic.main.loader_layout.view.*
import org.json.JSONObject

class SignUpFragment : BaseFragment() {

    override fun getLayout() = R.layout.fragment_sign_up

    override fun init() {
        rootView!!.logInTextView.setColor(
            "${getString(R.string.already_a_member)} ",
            ContextCompat.getColor(App.getInstance().applicationContext, R.color.colorText)
        )
        rootView!!.logInTextView.setColor(
            getString(R.string.sign_in),
            ContextCompat.getColor(App.getInstance().applicationContext, R.color.colorPrimary)
        )

        rootView!!.logInTextView.setOnClickListener {
            (activity as SignActivity).signPager.currentItem =
                (activity as SignActivity).signPager.currentItem - 1
        }

        rootView!!.signUpButton.setOnClickListener {
            signUp()
        }

        rootView!!.emailEditText.isEmailValid()
    }

    private fun signUp() {
        val email = rootView!!.emailEditText.text.toString()
        val fullName = rootView!!.fullNameEditText.text.toString()
        val password = rootView!!.passwordEditText.text.toString()
        val repeatPassword = rootView!!.repeatPasswordEditText.text.toString()

        if (email.isEmpty() || fullName.isEmpty() || password.isEmpty() || repeatPassword.isEmpty())
            Tools.createDialog(
                activity as SignActivity,
                "Error",
                getString(R.string.please_fill_all_fields)
            )
        else if (rootView!!.emailEditText.tag == "0")
            Tools.createDialog(activity as SignActivity, "Error", getString(R.string.invalid_email))
        else if (password != repeatPassword)
            Tools.createDialog(
                activity as SignActivity,
                "Error",
                getString(R.string.passwords_not_match)
            )
        else {
            val parameters = mutableMapOf<String, String>()
            parameters["email"] = email
            parameters["full_name"] = fullName
            parameters["password"] = password
            DataLoader.postRequest(
                rootView!!.loaderLayout,
                DataLoader.REGISTER,
                parameters,
                object : FutureCallback {
                    override fun onSuccess(response: String, status: Int) {
                        val json = JSONObject(response)
                        if (json.has("registered") && json.getBoolean("registered"))
                        {
                            (activity as SignActivity).signPager.currentItem =
                                (activity as SignActivity).signPager.currentItem - 1
                            Toast.makeText(activity as SignActivity, "Sign up was successful! Now sign in.",Toast.LENGTH_SHORT).show()
                        }
                    }

                    override fun onError(response: String, status: Int) {
                        Tools.createDialog(activity as SignActivity, "Error", response)
                    }

                    override fun onFail(response: String) {
                        Tools.createDialog(activity as SignActivity, "Error", "Server error, try again")
                    }
                })
        }
    }
}