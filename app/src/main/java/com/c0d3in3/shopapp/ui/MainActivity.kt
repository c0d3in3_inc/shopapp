package com.c0d3in3.shopapp.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.c0d3in3.shopapp.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
