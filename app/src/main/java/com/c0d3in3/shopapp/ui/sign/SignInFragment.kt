package com.c0d3in3.shopapp.ui.sign

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.c0d3in3.shopapp.App
import com.c0d3in3.shopapp.BaseFragment
import com.c0d3in3.shopapp.R
import com.c0d3in3.shopapp.extensions.setColor
import com.c0d3in3.shopapp.network.DataLoader
import com.c0d3in3.shopapp.network.DataLoader.LOG_IN
import com.c0d3in3.shopapp.network.FutureCallback
import com.c0d3in3.shopapp.ui.MainActivity
import com.c0d3in3.shopapp.utils.Tools
import com.c0d3in3.shopapp.utils.Tools.isValidEmail
import com.c0d3in3.shopapp.utils.UserPreferences
import com.c0d3in3.shopapp.utils.UserPreferences.USER_REF
import kotlinx.android.synthetic.main.activity_sign.*
import kotlinx.android.synthetic.main.fragment_sign_in.*
import kotlinx.android.synthetic.main.fragment_sign_in.view.*
import kotlinx.android.synthetic.main.fragment_sign_in.view.signUpTV
import org.json.JSONObject

class SignInFragment : BaseFragment() {

    private var rememberMe = false

    override fun getLayout() = R.layout.fragment_sign_in

    override fun init() {
        if(UserPreferences.readString(USER_REF)?.isNotEmpty()!!){
            rememberMe = true
            rootView!!.checkboxIV.setImageResource(R.mipmap.ic_checked)
        }
        else rootView!!.checkboxIV.setImageResource(R.mipmap.ic_unchecked)
        rootView!!.rememberMeLayout.setOnClickListener {
            rememberMe = if (rememberMe) {
                rootView!!.checkboxIV.setImageResource(R.mipmap.ic_unchecked)
                false
            } else {
                rootView!!.checkboxIV.setImageResource(R.mipmap.ic_checked)
                true
            }
        }

        rootView!!.emailET.addTextChangedListener(watcher)
        rootView!!.signUpTV.setColor("New user? ", ContextCompat.getColor(App.getInstance().applicationContext, R.color.colorText))
        rootView!!.signUpTV.setColor("${getString(R.string.sign_up)} ", ContextCompat.getColor(App.getInstance().applicationContext, R.color.colorPrimary))
        rootView!!.signUpTV.setColor("here", ContextCompat.getColor(App.getInstance().applicationContext, R.color.colorText))

        rootView!!.signUpTV.setOnClickListener {
            (activity as SignActivity).signPager.currentItem = (activity as SignActivity).signPager.currentItem + 1
        }

        rootView!!.signInButton.setOnClickListener {
            signIn()
        }
    }

    private fun signIn(){
        if(!isValidEmail(emailET.text.toString())) return Tools.createDialog(activity as SignActivity, "Error", "Email is not valid!")
        if(passwordET.text.toString().isBlank()) return Tools.createDialog(activity as SignActivity, "Error", "Password is empty")
        val params = mutableMapOf<String, String>()
        params["email"] = emailET.text.toString()
        params["password"] = passwordET.text.toString()
        DataLoader.postRequest(LOG_IN, params, object: FutureCallback{
            override fun onSuccess(response: String, status: Int) {
                val json = JSONObject(response)
                if(rememberMe)
                    UserPreferences.insertString(USER_REF, json.getString("token"))
                val flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                Tools.createActivity(activity as SignActivity, MainActivity(), null, flags)
            }

            override fun onError(response: String, status: Int) {
                Tools.createDialog(activity as SignActivity, "Error", "User not found")
            }

            override fun onFail(response: String) {
                Tools.createDialog(activity as SignActivity, "Error", "Server error, try again")
            }

        })
    }


    private val watcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {

        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            if (isValidEmail(s))
                rootView!!.emailET.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, App.getInstance().applicationContext.getDrawable(R.drawable.valid_drawable), null)
            else
                rootView!!.emailET.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null)
        }

    }
}