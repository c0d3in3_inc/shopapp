package com.c0d3in3.shopapp.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.c0d3in3.shopapp.R
import com.c0d3in3.shopapp.ui.sign.SignActivity
import com.c0d3in3.shopapp.utils.Tools
import com.c0d3in3.shopapp.utils.UserPreferences

class SplashActivity : AppCompatActivity() {

    private val handler = Handler()
    private val runnable = Runnable {
        init()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
    }

    private fun init() {
        val flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        if (UserPreferences.readString(UserPreferences.USER_REF)!!.isNotEmpty())
            Tools.createActivity(this, MainActivity(), null, flags)
        else
            Tools.createActivity(this,
                SignActivity(), null, flags)
    }

    override fun onStart() {
        startSplash()
        super.onStart()
    }

    override fun onPause() {
        stopSplash()
        super.onPause()
    }

    private fun startSplash() {
        handler.postDelayed(runnable, 3000)
    }

    private fun stopSplash() {
        handler.removeCallbacks(runnable)
    }
}
