package com.c0d3in3.shopapp.ui.profile

import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.c0d3in3.shopapp.R
import com.c0d3in3.shopapp.utils.Tools
import com.c0d3in3.shopapp.utils.Tools.USER_PERMISSIONS_REQUEST_CODE
import kotlinx.android.synthetic.main.activity_complete_profile.*

class CompleteProfileActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_complete_profile)

        init()
    }

    private fun init(){
        choosePhotoLayout.setOnClickListener {
            choosePhotoOnClick()
        }
    }


    private fun choosePhotoOnClick() {
        if (checkPermission(Manifest.permission.READ_EXTERNAL_STORAGE) && checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) && checkPermission(
                Manifest.permission.CAMERA
            )
        ) choosePhoto()
        else
            requestPermissions()
    }

    private fun choosePhoto() {

    }

    private fun requestPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(
                arrayOf(
                    Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                ), Tools.USER_PERMISSIONS_REQUEST_CODE
            )
        }
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == USER_PERMISSIONS_REQUEST_CODE) {
            if (permissions.size == grantResults.size) {
                choosePhoto()
            }
            else requestPermissions()
        }
    }

    private fun checkPermission(permission: String) =
        ActivityCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED


}
