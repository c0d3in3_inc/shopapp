package com.c0d3in3.shopapp.ui.sign

import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Patterns
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.c0d3in3.shopapp.R
import com.c0d3in3.shopapp.extensions.setColor
import kotlinx.android.synthetic.main.activity_sign.*


class SignActivity : AppCompatActivity() {

    private var rememberMe = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign)

        init()
    }

    private fun init() {
        val pagerAdapter = SignViewPager(this)
        pagerAdapter.addFragment(SignInFragment())
        pagerAdapter.addFragment(SignUpFragment())
        signPager.adapter = pagerAdapter
    }

    override fun onBackPressed() {
        if (signPager.currentItem == 0)
            super.onBackPressed()
        else
            signPager.currentItem = signPager.currentItem - 1
    }
}
