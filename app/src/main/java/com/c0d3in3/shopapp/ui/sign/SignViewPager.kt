package com.c0d3in3.shopapp.ui.sign

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter

class SignViewPager(fragmentActivity: FragmentActivity) :
    FragmentStateAdapter(fragmentActivity) {

    private val fragmentsList = arrayListOf<Fragment>()

    override fun getItemCount() = fragmentsList.size

    fun addFragment(fragment: Fragment){
        fragmentsList.add(fragment)
    }

    override fun createFragment(position: Int) = fragmentsList[position]
}